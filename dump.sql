-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 10, 2018 at 11:37 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e-nihongo`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id` int(10) UNSIGNED NOT NULL,
  `examples_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `id_option` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `answers` text COLLATE utf8_unicode_ci NOT NULL,
  `ans_status` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `examples_id`, `question_id`, `id_option`, `user_id`, `answers`, `ans_status`, `option_id`, `created_at`, `updated_at`) VALUES
(1, 8, 1, 1, 15, '2', 1, 0, '2017-06-21 05:10:43', '2017-06-21 05:10:43'),
(2, 8, 2, 1, 15, '2', 0, 0, '2017-06-21 05:10:44', '2017-06-21 05:10:44'),
(3, 8, 1, 2, 15, '2', 1, 0, '2017-06-21 06:09:41', '2017-06-21 06:09:41'),
(4, 8, 2, 2, 15, '4', 0, 0, '2017-06-21 06:09:41', '2017-06-21 06:09:41'),
(5, 8, 1, 3, 15, '2', 1, 0, '2017-06-21 07:39:55', '2017-06-21 07:39:55'),
(6, 8, 2, 3, 15, '8', 1, 0, '2017-06-21 07:39:55', '2017-06-21 07:39:55'),
(7, 8, 1, 4, 15, '2', 1, 0, '2017-06-21 07:56:58', '2017-06-21 07:56:58'),
(8, 8, 2, 4, 15, '5', 0, 0, '2017-06-21 07:56:58', '2017-06-21 07:56:58'),
(9, 8, 1, 5, 15, '2', 1, 0, '2017-06-21 07:57:50', '2017-06-21 07:57:50'),
(10, 8, 2, 5, 15, '8', 1, 0, '2017-06-21 07:57:50', '2017-06-21 07:57:50'),
(11, 8, 1, 6, 15, '1', 0, 0, '2017-06-21 08:00:13', '2017-06-21 08:00:13'),
(12, 8, 2, 6, 15, '7', 0, 0, '2017-06-21 08:00:13', '2017-06-21 08:00:13'),
(13, 8, 1, 7, 15, '2', 1, 0, '2017-06-21 23:50:44', '2017-06-21 23:50:44'),
(14, 8, 2, 7, 15, '8', 1, 0, '2017-06-21 23:50:44', '2017-06-21 23:50:44'),
(15, 8, 1, 8, 15, '2', 1, 0, '2017-06-21 23:54:01', '2017-06-21 23:54:01'),
(16, 8, 2, 8, 15, '6', 0, 0, '2017-06-21 23:54:01', '2017-06-21 23:54:01'),
(17, 8, 1, 9, 15, '1', 0, 0, '2017-06-21 23:54:13', '2017-06-21 23:54:13'),
(18, 8, 2, 9, 15, '6', 0, 0, '2017-06-21 23:54:13', '2017-06-21 23:54:13'),
(19, 8, 1, 10, 15, '2', 1, 0, '2017-06-21 23:55:05', '2017-06-21 23:55:05'),
(20, 8, 2, 10, 15, '8', 1, 0, '2017-06-21 23:55:05', '2017-06-21 23:55:05'),
(21, 8, 1, 11, 16, '2', 1, 0, '2017-06-26 05:16:42', '2017-06-26 05:16:42'),
(22, 8, 2, 11, 16, '8', 1, 0, '2017-06-26 05:16:42', '2017-06-26 05:16:42'),
(23, 8, 1, 12, 16, '2', 1, 0, '2017-06-26 05:24:28', '2017-06-26 05:24:28'),
(24, 8, 2, 12, 16, '8', 1, 0, '2017-06-26 05:24:28', '2017-06-26 05:24:28'),
(25, 9, 3, 13, 16, '7', 0, 0, '2017-06-26 20:06:44', '2017-06-26 20:06:44'),
(26, 9, 4, 13, 16, '4', 0, 0, '2017-06-26 20:06:44', '2017-06-26 20:06:44'),
(27, 9, 5, 13, 16, '5', 0, 0, '2017-06-26 20:06:44', '2017-06-26 20:06:44'),
(28, 9, 6, 13, 16, '8', 0, 0, '2017-06-26 20:06:44', '2017-06-26 20:06:44'),
(29, 9, 7, 13, 16, '9', 0, 0, '2017-06-26 20:06:44', '2017-06-26 20:06:44'),
(30, 9, 3, 14, 16, '3', 0, 0, '2017-06-26 20:08:56', '2017-06-26 20:08:56'),
(31, 9, 4, 14, 16, '6', 0, 0, '2017-06-26 20:08:56', '2017-06-26 20:08:56'),
(32, 9, 5, 14, 16, '1', 0, 0, '2017-06-26 20:08:56', '2017-06-26 20:08:56'),
(33, 9, 6, 14, 16, '9', 0, 0, '2017-06-26 20:08:56', '2017-06-26 20:08:56'),
(34, 9, 7, 14, 16, '4', 0, 0, '2017-06-26 20:08:56', '2017-06-26 20:08:56'),
(35, 9, 3, 15, 16, '3', 1, 0, '2017-06-27 01:52:30', '2017-06-29 04:26:15'),
(36, 9, 4, 15, 16, 'aaa', 0, 0, '2017-06-27 01:52:30', '2017-06-27 01:52:30'),
(37, 9, 5, 15, 16, '14-aa', 1, 0, '2017-06-27 01:52:30', '2017-06-29 04:04:04'),
(38, 9, 6, 15, 16, '0-aa', 1, 0, '2017-06-27 01:52:30', '2017-06-29 04:04:04'),
(39, 9, 7, 15, 16, '8', 1, 0, '2017-06-27 01:52:30', '2017-06-29 04:16:48'),
(40, 8, 1, 16, 16, '1', 0, 0, '2017-06-30 07:56:17', '2017-06-30 07:56:17'),
(41, 8, 2, 16, 16, '8', 1, 0, '2017-06-30 07:56:17', '2017-06-30 07:56:17'),
(42, 8, 1, 17, 16, '2', 1, 0, '2017-06-30 07:57:29', '2017-06-30 07:57:29'),
(43, 8, 2, 17, 16, '6', 0, 0, '2017-06-30 07:57:29', '2017-06-30 07:57:29');

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `id` int(10) UNSIGNED NOT NULL,
  `bank_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bank_owner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bank_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `bank_name`, `bank_owner`, `bank_number`, `image`, `created_at`, `updated_at`) VALUES
(2, 'ธนาคารกรุงเทพ', 'ปริญญา เขื่อนควบ', '1111-2545-1200', '1490463855.png', '2017-03-25 07:25:50', '2017-03-25 10:44:15'),
(3, 'ธนาคารกสิกร', 'ปริญญา เขื่อนควบ', '1111-2545-1254', '1490451979.png', '2017-03-25 07:26:19', '2017-03-25 07:26:19'),
(4, 'ธนาคารไทยพานิช', 'ปริญญา เขื่อนควบ', '1111-2545-1254', '1490452004.png', '2017-03-25 07:26:44', '2017-03-25 07:26:44');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_blog` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `detail_blog` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `view` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title_blog`, `detail_blog`, `image`, `created_at`, `updated_at`, `view`) VALUES
(1, 'พาเดินงาน Startup Thailand 2016 ชมบูตสตาร์ทอัพที่น่าสนใจในงานวันแรก', '<p style=\"margin-bottom: 0px; padding: 0.4em 0px; border: 0px; outline: 0px; vertical-align: baseline; line-height: inherit; color: rgb(51, 51, 51); font-family: Helvetica, Arial, Thonburi, Tahoma, FreeSans, sans-serif;\">งาน Startup Thailand 2016 ที่ศูนย์ประชุมแห่งชาติสิริกิต์ เปิดตัวธุรกิจสตาร์ทอัพของไทยกว่า 200 ราย พร้อมทั้งหน่วยงานสนับสนุนสตาร์ทอัพจากต่างประเทศ เป้าหมายของงานคือ เสริมสร้างความแข็งแกร่งให้สตาร์ทอัพไทย และสร้างแรงบันดาลใจให้ผู้ประกอบการที่สนใจสร้างสตาร์ทอัพ</p><p style=\"margin-bottom: 0px; padding: 0.4em 0px; border: 0px; outline: 0px; vertical-align: baseline; line-height: inherit; color: rgb(51, 51, 51); font-family: Helvetica, Arial, Thonburi, Tahoma, FreeSans, sans-serif;\">จึงเก็บบรรยากาศและตัวอย่างบริษัทสตาร์ทอัพส่วนหนึ่ง (มีเยอะมาก) ในงานวันแรก (28 เมษายน) มาให้ชมกัน</p><p style=\"margin-bottom: 0px; padding: 0.4em 0px; border: 0px; outline: 0px; vertical-align: baseline; line-height: inherit; color: rgb(51, 51, 51); font-family: Helvetica, Arial, Thonburi, Tahoma, FreeSans, sans-serif;\">ที่เห็นโดดเด่นหน้าฮอลล์จัดงานคือบูธของ&nbsp;<a href=\"https://www.facebook.com/makerspaceth/\" style=\"margin: 0px; padding: 0px; border: 0px; outline-style: initial; outline-width: 0px; vertical-align: baseline; color: rgb(29, 95, 179); background-color: rgb(255, 255, 255);\">Makerspace</a>&nbsp;บริษัทที่ระบุว่าเป็นชุมชนที่ช่วยสร้างสตาร์ทอัพ เป็นพื้นที่ที่ช่วยให้ผู้อยากริเริ่มธุรกิจสตาร์ทอัพ สามาถถ่ายทอดไอเดียได้ออกมาเป็นรูปเป็นร่างมากขึ้น นอกเหนือจากไอเดียแล้วยังมีอุปกรณ์ครบครันที่ผู้ริเริ่มสตาร์ทอัพต้องการ โดยในบูธมีผลงานตัวอย่างของทางบริษัททั้ง 3D printer โดรน เครื่องทอผ้า ฯลฯ</p><p style=\"margin-bottom: 0px; padding: 0.4em 0px; border: 0px; outline: 0px; vertical-align: baseline; line-height: inherit; color: rgb(51, 51, 51); font-family: Helvetica, Arial, Thonburi, Tahoma, FreeSans, sans-serif;\">เมื่อเข้าประตูใหญ่สิ่งแรกที่เจอคือ บูธให้ความรู้เกี่ยวกับสตาร์ทอัพ เหมาะสำหรับผู้ที่เริ่มสนใจ นอกจากนี้ยังรวบรวบข้อมูลสตาร์ทอัพที่ประสบความสำเร็จทั้งระดับโลกและในประเทศไทย เช่น Uber, Airbnb, Grab ส่วนของไทยเองก็มี Wongnai, Ookbee เป็นต้น</p><p style=\"margin-bottom: 0px; padding: 0.4em 0px; border: 0px; outline: 0px; vertical-align: baseline; line-height: inherit; color: rgb(51, 51, 51); font-family: Helvetica, Arial, Thonburi, Tahoma, FreeSans, sans-serif;\">ประเดิมบูธแรกที่จะนำเสนอให้ชมกันคือ&nbsp;<a href=\"http://localalike.com/\" style=\"margin: 0px; padding: 0px; border: 0px; outline-style: initial; outline-width: 0px; vertical-align: baseline; color: rgb(29, 95, 179); background-color: rgb(255, 255, 255);\">LocalAlike</a>&nbsp;ทำทัวร์ท่องเที่ยวเข้าถึงชุมชน เป็นทัวร์ท่องเที่ยวทางเลือกที่เม็ดเงินกระจายไปถึงคนท้องถิ่นมากขึ้น LocalAlike ใช้ช่องทางออนไลน์ในการหาลูกค้านักท่องเที่ยวมาได้ห้าปีแล้ว แต่เพิ่งก่อตั้งเว็บไซต์และแพลตฟอร์มต่างๆ ได้ไม่กี่เดือน</p><p style=\"margin-bottom: 0px; padding: 0.4em 0px; border: 0px; outline: 0px; vertical-align: baseline; line-height: inherit; color: rgb(51, 51, 51); font-family: Helvetica, Arial, Thonburi, Tahoma, FreeSans, sans-serif;\">สตาร์ทอัพเพื่อการท่องเที่ยวอีกแห่งหนึ่งที่น่าสนใจคือ&nbsp;<a href=\"https://www.takemetour.com/\" style=\"margin: 0px; padding: 0px; border: 0px; outline-style: initial; outline-width: 0px; vertical-align: baseline; color: rgb(29, 95, 179); background-color: rgb(255, 255, 255);\">TakeMeTour</a>&nbsp;ต่างกับ LocalAlike ตรงที่เป็น 1-day trip เท่านั้น ไม่ได้จัดการเรื่องที่พักให้ บริษัทเปิดโอกาสให้ผู้ที่ไม่ได้เป็นไกด์มืออาชีพ แต่อยากจะพานักท่องเที่ยวไปเที่ยวในสถานที่ในละแวกบ้านที่น่าสนใจแต่ยังไม่ได้รับความนิยม ได้พานักท่องเที่ยวไปเที่ยวและยังได้รายได้เสริมด้วย</p>', '1492569462-3e8450400f8f95049a9e2a3423c4d02a.jpg', '2017-04-18 06:25:09', '2018-05-09 09:23:03', 86),
(2, 'พาเดินงาน LAUNCH Festival 2017 งานสตาร์ตอัพระดับโลก', '<p>กระแสการเปิดบริษัทสตาร์ตอัพบ้านเราอาจจะเพิ่งได้รับความนิยมสูงๆ ในช่วงไม่กี่ปีมานี้จนมีงานประกวดในไทยอยู่เนืองๆ แต่งานในสหรัฐฯ ที่มีสตาร์ตอัพจำนวนมาก ก็มีการประกวดกันมานานแล้ว หนึ่งในงานที่ได้รับความนิยมเป็นอย่างสูงคืองาน LAUNCH Festival ที่มีการแข่งขันนำเสนอธุรกิจ (pitch) กันเป็นจำนวนมาก ผู้ร่วมงานปีละนับหมื่นคน</p><p>งานนี้ทาง Digital Ventures และ IBM ก็เชิญ Blognone ไปร่วมงาน ผมจึงมีโอกาสเก็บบรรยากาศมาเล่าให้ฟังกันครับ</p><p>ตัวงานแบ่งออกเป็นพื้นที่จัดแสดงนิทรรศการ ให้พื้นที่กับสตาร์ตอัพที่มาร่วมงาน กับเวทีการนำเสนอธุรกิจ ที่มีแยกกันสามเวที แต่ละเวทีก็จะมีช่วงเวลาเสวนาในช่วงเช้า พูดคุยถึงประเด็นต่างๆ ในวงการสตาร์ตอัพ ตั้งแต่ความเท่าเทียมทางเพศ การที่หุ่นยนต์จะเข้ามาแย่งงานมนุษย์ และแนวทางที่บริษัท venture capital เลือกลงทุนในบริษัทต่างๆ</p><p>ส่วนนิทรรศการงานนี้ IBM เป็นสปอนเซอร์ใหญ่ (เวทีหลักก็เป็นเวทีไอบีเอ็ม) บูตหลักเป็นบูตไอบีเอ็มมาโชว์พลังของ Watson โดยทำแอปพลิเคชั่นตัวอย่าง เป็นแอปแนะนำเบียร์ตามลักษณะของคนดื่ม</p><p>การนำเสนอธุรกิจ</p><p><br></p><p>เนื่องจากเป็นงานใหญ่ บริษัทที่มาร่วมนำเสนอจึงมีเยอะมาก บนเวทีมีการจัดการเวลาค่อนข้างเข้มงวด โดยแต่ละทีมจะมีเวลานำเสนอ 3 นาทีพอดีเท่านั้น หากเกินไปประมาณ 10 วินาทีไมโครโฟนก็จะตัด ถัดจากนั้นเป็นช่วงเวลาถามตอบกับกรรมการอีก 4 นาทีรวมทั้งกรรมการพูดและคนเข้าแข่งขันตอบ โดยการแข่งจะแบ่งเป็นรอบๆ รอบละ 5 บริษัทแล้วให้กรรมการแต่ละชุดชุดละ 5 คนจากบริษัทลงทุนขึ้นเวทีให้คะแนน</p><p>แนวคำถามคล้ายกับการแข่งขันที่เราเห็นในบ้านเรา คำถามส่วนมากจะถามถึงการแข่งขัน, มูลค่ารวมในตลาดอนาคต, แนวทางการทำเงิน, ฐานผู้ใช้ ฯลฯ โดยทั่วไปแล้วเวลาในการนำเสนอค่อนข้างสั้น พิธีกรเองจะเตือนกรรมการว่าเวลาสามนาทีไม่เพียงพอต่อการให้ข้อมูลทุกอย่างแน่นอน และตัวกรรมการเองก็จะละหัวข้อบางอย่างไป หรือบางบริษัทที่เป็นธุรกิจดำเนินการไปแล้วก็อาจจะปฏิเสธไม่ตอบคำถามบางคำถามไปเลย เพราะเปิดเผยต่อสาธารณะไม่ได้</p><p>ช่วงประกาศผลของแต่ละรอบจะค่อนข้างเร็ว เพราะพิธีกรจะขอให้กรรมการแต่ละคนบอกบริษัทอันดับ 2 และ 3 ก่อน ทีละคน จากนั้นจึงบอกบริษัทอันดับ 1 ของแต่ละคนอีกครั้ง (บอกสองรอบให้มีลุ้นว่าใครจะได้ที่หนึ่งมากที่สุด) และคิดคะแนนจากอันดับของกรรมการแต่ละคน กรรมการไม่มีโอกาสปรึกษากันจึงจบได้ในเวลาอันรวดเร็ว</p><p>ตัวอย่างการนำเสนอบางบริษัทในงาน</p>', '1492570137-c9c344dc18456caeafda8c55091fa938.jpg', '2017-04-18 19:48:57', '2017-04-20 23:51:22', 17);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name_category`, `created_at`, `updated_at`) VALUES
(1, 'ไวยากรณ์', '2017-04-29 03:53:01', '2017-04-29 03:53:01'),
(2, 'คำศัพท์', '2017-04-29 03:53:10', '2017-04-29 03:53:10'),
(3, 'คันจิ.', '2017-04-29 03:53:14', '2017-04-29 03:56:44'),
(4, 'การสื่อสาร', '2017-04-29 03:56:59', '2017-04-29 03:56:59'),
(5, 'การอ่าน', '2017-04-29 03:57:03', '2017-04-29 03:57:03'),
(6, ' การเขียน', '2017-04-29 03:57:08', '2017-04-29 03:57:08'),
(7, ' การประยุกต์ใช้', '2017-04-29 03:57:16', '2017-04-29 03:57:16'),
(8, ' ญี่ปุ่นศึกษา', '2017-04-29 03:57:21', '2017-04-29 03:57:21');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `course_id`, `comment`, `created_at`, `updated_at`) VALUES
(1, 9, 4, 'ไปเรียน 6 คนมีส่วนลดไหมครับ >< 555+', '2017-04-14 12:54:00', '2017-04-14 13:40:12'),
(7, 10, 4, 'Say Hi!', '2017-04-14 13:43:59', '2017-04-14 13:43:59'),
(8, 10, 5, '55555++', '2017-04-14 13:45:50', '2017-04-14 13:45:50'),
(9, 9, 5, 'as', '2017-04-29 15:50:03', '2017-04-29 15:50:03'),
(10, 15, 4, '888888', '2017-05-26 22:31:53', '2017-05-26 22:31:53');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `phone`, `detail`, `created_at`, `updated_at`) VALUES
(1, 'kimkundad', 'kim.kundad@gmail.com', '0811007753', 'สอบถามรายละเอียด 555+++', '2017-04-16 09:01:17', '2017-04-16 09:01:17');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `type_course` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_course` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `detail_course` text COLLATE utf8_unicode_ci NOT NULL,
  `price_course` int(11) NOT NULL,
  `start_course` date NOT NULL,
  `end_course` date NOT NULL,
  `time_course` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `day_course` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_course` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url_course` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `discount` int(11) NOT NULL,
  `code_course` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `department_id` int(11) NOT NULL,
  `ch_status` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `user_id`, `type_course`, `title_course`, `detail_course`, `price_course`, `start_course`, `end_course`, `time_course`, `day_course`, `image_course`, `url_course`, `created_at`, `updated_at`, `discount`, `code_course`, `department_id`, `ch_status`) VALUES
(6, 1, '', 'นูเบีย Z17S สีน้ำเงิน ออโรร่า', 'รายละเอียดสินค้า นูเบีย Z17S สีน้ำเงิน ออโรร่า มือถือสเปคแรง แรม8GB **ผ่อนเดือนละ 1799บาท\r\nnubia Z17S Aurora Blue ram8/rom128 Snapdragon 835\r\nเพื่อป้องกันความสับสนและความเข้าใจผิดของผู้ซื้อ\r\nทางร้านค้านูเบียสมาร์ทโฟน ขอเรียนให้ทุกท่านทราบว่ารุ่นที่จัดจำหน่ายเป็น เวอร์ชั่นสากล (ศูนย์ไทย) โดยมีจุดสังเกตุดังนี้\r\n     1. กล่องสีขาว\r\n     2. อแดปเตอร์สีขาว กำลังไฟ 3.0A\r\n     3. ฝาหลังเครื่องสีน้ำเงินเฉดเข้ม\r\n     5. สเปคเครื่อง คุณสมบัติพื้นฐาน เหมือนกันทุกประการ', 15900, '0000-00-00', '0000-00-00', '', '', '1525855462.jpg', '', '2018-05-09 08:44:22', '2018-05-09 08:44:22', 0, 'Z17S', 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_department` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_department` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name_department`, `image_department`, `created_at`, `updated_at`) VALUES
(5, 'อุปกรณ์ อิเล็กทรอนิกส์', '1525854798.jpg', '2018-05-09 08:33:18', '2018-05-09 08:33:18'),
(6, 'สุขภาพและความงาม', '1525854950.jpg', '2018-05-09 08:35:50', '2018-05-09 08:35:50'),
(7, 'เด็กอ่อน และของเล่น', '1525854997.jpg', '2018-05-09 08:36:37', '2018-05-09 08:36:37'),
(8, 'ซูเปอร์มาร์เก็ต และสัตว์เลี้ยง', '1525855041.jpg', '2018-05-09 08:37:21', '2018-05-09 08:37:21'),
(9, 'บ้านและไลฟ์สไตล์', '1525855113.jpg', '2018-05-09 08:38:33', '2018-05-09 08:38:33'),
(10, 'แฟชั่นผู้หญิง', '1525855162.jpg', '2018-05-09 08:39:22', '2018-05-09 08:39:22');

-- --------------------------------------------------------

--
-- Table structure for table `examples`
--

CREATE TABLE `examples` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `examples_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `examples_detail` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `examples`
--

INSERT INTO `examples` (`id`, `category_id`, `course_id`, `examples_name`, `examples_detail`, `created_at`, `updated_at`) VALUES
(8, 1, 4, '[ข้อสอบจริง JP101] *คันจิ ติวสอบ N1', '[ข้อสอบจริง JP101] *คันจิ ติวสอบ N1', '2017-05-19 07:26:28', '2017-05-19 07:39:27'),
(9, 6, 4, '[ข้อสอบจริง JP101] *คันจิ ติวสอบ N1 การเขียน', '自然が滅んでいくのを防ぐには、嘆くだけではいけない。これ以上の悲惨な状況を避けるために行動を起こそう。', '2017-05-19 07:40:03', '2017-05-19 07:40:03'),
(10, 2, 5, 'แบบทดสอบคำศัพท์ – คันจิ สำหรับเตรียมสอบวัดระดับภาษาญี่ปุ่น JLPT', 'วันนี้มีแบบทดสอบคำศัพท์ – คันจิ สำหรับเตรียมสอบวัดระดับภาษาญี่ปุ่น JLPT มาฝากนะคะ มีของทุกระดับเลย ตั้งแต่ N1, N2, N3, N4, N5 เป็นคำศัพท์และคันจิที่ควรรู้ในการสอบวัดระดับนะคะ เนื่องจากนำเอาไปออกสอบบ่อย ใครยังไม่แน่ใจว่าความรู้อยู่ระดับไหนหรือไม่มั่นใจว่าจะสอบข้ามระดับไหวไหม ลองพิจารณาความรู้ของตัวเองจากบททดสอบอันนี้ได้นะคะ แต่ละระดับคะแนนเต็ม 10 คะแนนค่า\r\nเริ่มทำข้อสอบได้ค่ะ (ควรใช้เวลาไม่เกิน 3 นาที ในแต่ละพาร์ท)', '2017-05-21 02:46:02', '2017-05-21 02:46:02'),
(11, 3, 5, 'ฝึกทำข้อสอบวัดระดับ N3 พาร์ทคำศัพท์ คันจิ', 'ฝึกทำข้อสอบวัดระดับ N3 ออนไลน์ฟรี EP.5 (JLPT Vocabulary Kanji พาร์ทคำศัพท์ คันจิ)\r\nลองฝึกทำข้อสอบฟรี! ได้ที่นี่…', '2017-05-21 02:50:39', '2017-05-21 02:50:39'),
(12, 4, 5, '[ข้อสอบจริง JP101] *คันจิ ติวสอบ N8', '5555', '2017-05-26 22:06:41', '2017-05-26 22:06:41');

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE `levels` (
  `id` int(10) UNSIGNED NOT NULL,
  `points` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`id`, `points`, `created_at`, `updated_at`) VALUES
(1, 0, '2016-03-16 23:41:00', '2016-03-16 23:41:00'),
(2, 100, '2017-03-16 23:41:00', '2017-03-16 23:41:00'),
(3, 200, '2018-03-16 23:41:00', '2018-03-16 23:41:00'),
(4, 250, '2019-03-16 23:41:00', '2019-03-16 23:41:00'),
(5, 300, '2020-03-16 23:41:00', '2020-03-16 23:41:00'),
(6, 350, '2021-03-16 23:41:00', '2021-03-16 23:41:00'),
(7, 400, '2022-03-16 23:41:00', '2022-03-16 23:41:00'),
(8, 450, '2023-03-16 23:41:00', '2023-03-16 23:41:00'),
(9, 500, '2024-03-16 23:41:00', '2024-03-16 23:41:00'),
(10, 550, '2025-03-16 23:41:00', '2025-03-16 23:41:00'),
(11, 600, '2026-03-16 23:41:00', '2026-03-16 23:41:00'),
(12, 650, '2027-03-16 23:41:00', '2027-03-16 23:41:00'),
(13, 700, '2028-03-16 23:41:00', '2028-03-16 23:41:00'),
(14, 750, '2029-03-16 23:41:00', '2029-03-16 23:41:00'),
(15, 800, '2030-03-16 23:41:00', '2030-03-16 23:41:00'),
(16, 850, '2031-03-16 23:41:00', '2031-03-16 23:41:00'),
(17, 900, '2001-04-16 23:41:00', '2001-04-16 23:41:00'),
(18, 950, '2002-04-16 23:41:00', '2002-04-16 23:41:00'),
(19, 1000, '2003-04-16 23:41:00', '2003-04-16 23:41:00'),
(20, 1050, '2004-04-16 23:41:00', '2004-04-16 23:41:00'),
(21, 1100, '2005-04-16 23:41:00', '2005-04-16 23:41:00'),
(22, 1150, '2006-04-16 23:41:00', '2006-04-16 23:41:00'),
(23, 1200, '2007-04-16 23:41:00', '2007-04-16 23:41:00'),
(24, 1250, '2008-04-16 23:41:00', '2008-04-16 23:41:00'),
(25, 1300, '2009-04-16 23:41:00', '2009-04-16 23:41:00'),
(26, 1350, '2010-04-16 23:41:00', '2010-04-16 23:41:00'),
(27, 1400, '2011-04-16 23:41:00', '2011-04-16 23:41:00'),
(28, 1450, '2012-04-16 23:41:00', '2012-04-16 23:41:00'),
(29, 1500, '2013-04-16 23:41:00', '2013-04-16 23:41:00'),
(30, 1550, '2014-04-16 23:41:00', '2014-04-16 23:41:00'),
(31, 1600, '2015-04-16 23:41:00', '2015-04-16 23:41:00'),
(32, 1650, '2016-04-16 23:41:00', '2016-04-16 23:41:00'),
(33, 1700, '2017-04-16 23:41:00', '2017-04-16 23:41:00'),
(34, 1750, '2018-04-16 23:41:00', '2018-04-16 23:41:00'),
(35, 1800, '2019-04-16 23:41:00', '2019-04-16 23:41:00'),
(36, 1850, '2020-04-16 23:41:00', '2020-04-16 23:41:00'),
(37, 1900, '2021-04-16 23:41:00', '2021-04-16 23:41:00'),
(38, 1950, '2022-04-16 23:41:00', '2022-04-16 23:41:00'),
(39, 2000, '2023-04-16 23:41:00', '2023-04-16 23:41:00'),
(40, 2050, '2024-04-16 23:41:00', '2024-04-16 23:41:00'),
(41, 2100, '2025-04-16 23:41:00', '2025-04-16 23:41:00'),
(42, 2150, '2026-04-16 23:41:00', '2026-04-16 23:41:00'),
(43, 2200, '2027-04-16 23:41:00', '2027-04-16 23:41:00'),
(44, 2250, '2028-04-16 23:41:00', '2028-04-16 23:41:00'),
(45, 2300, '2029-04-16 23:41:00', '2029-04-16 23:41:00'),
(46, 2350, '2030-04-16 23:41:00', '2030-04-16 23:41:00'),
(47, 2400, '2001-05-16 23:41:00', '2001-05-16 23:41:00'),
(48, 2450, '2002-05-16 23:41:00', '2002-05-16 23:41:00'),
(49, 2500, '2003-05-16 23:41:00', '2003-05-16 23:41:00'),
(50, 2550, '2004-05-16 23:41:00', '2004-05-16 23:41:00'),
(51, 2600, '2005-05-16 23:41:00', '2005-05-16 23:41:00'),
(52, 2650, '2006-05-16 23:41:00', '2006-05-16 23:41:00'),
(53, 2700, '2007-05-16 23:41:00', '2007-05-16 23:41:00'),
(54, 2750, '2008-05-16 23:41:00', '2008-05-16 23:41:00'),
(55, 2800, '2009-05-16 23:41:00', '2009-05-16 23:41:00'),
(56, 2850, '2010-05-16 23:41:00', '2010-05-16 23:41:00'),
(57, 2900, '2011-05-16 23:41:00', '2011-05-16 23:41:00'),
(58, 2950, '2012-05-16 23:41:00', '2012-05-16 23:41:00'),
(59, 3000, '2013-05-16 23:41:00', '2013-05-16 23:41:00'),
(60, 3050, '2014-05-16 23:41:00', '2014-05-16 23:41:00'),
(61, 3100, '2015-05-16 23:41:00', '2015-05-16 23:41:00'),
(62, 3150, '2016-05-16 23:41:00', '2016-05-16 23:41:00'),
(63, 3200, '2017-05-16 23:41:00', '2017-05-16 23:41:00'),
(64, 3250, '2018-05-16 23:41:00', '2018-05-16 23:41:00'),
(65, 3300, '2019-05-16 23:41:00', '2019-05-16 23:41:00'),
(66, 3350, '2020-05-16 23:41:00', '2020-05-16 23:41:00'),
(67, 3400, '2021-05-16 23:41:00', '2021-05-16 23:41:00'),
(68, 3450, '2022-05-16 23:41:00', '2022-05-16 23:41:00'),
(69, 3500, '2023-05-16 23:41:00', '2023-05-16 23:41:00'),
(70, 3550, '2024-05-16 23:41:00', '2024-05-16 23:41:00'),
(71, 3600, '2025-05-16 23:41:00', '2025-05-16 23:41:00'),
(72, 3650, '2026-05-16 23:41:00', '2026-05-16 23:41:00'),
(73, 3700, '2027-05-16 23:41:00', '2027-05-16 23:41:00'),
(74, 3750, '2028-05-16 23:41:00', '2028-05-16 23:41:00'),
(75, 3800, '2029-05-16 23:41:00', '2029-05-16 23:41:00'),
(76, 3850, '2030-05-16 23:41:00', '2030-05-16 23:41:00'),
(77, 3900, '2031-05-16 23:41:00', '2031-05-16 23:41:00'),
(78, 3950, '2001-06-16 23:41:00', '2001-06-16 23:41:00'),
(79, 4000, '2002-06-16 23:41:00', '2002-06-16 23:41:00'),
(80, 4050, '2003-06-16 23:41:00', '2003-06-16 23:41:00'),
(81, 4100, '2004-06-16 23:41:00', '2004-06-16 23:41:00'),
(82, 4150, '2005-06-16 23:41:00', '2005-06-16 23:41:00'),
(83, 4200, '2006-06-16 23:41:00', '2006-06-16 23:41:00'),
(84, 4250, '2007-06-16 23:41:00', '2007-06-16 23:41:00'),
(85, 4300, '2008-06-16 23:41:00', '2008-06-16 23:41:00'),
(86, 4350, '2009-06-16 23:41:00', '2009-06-16 23:41:00'),
(87, 4400, '2010-06-16 23:41:00', '2010-06-16 23:41:00'),
(88, 4450, '2011-06-16 23:41:00', '2011-06-16 23:41:00'),
(89, 4500, '2012-06-16 23:41:00', '2012-06-16 23:41:00'),
(90, 4550, '2013-06-16 23:41:00', '2013-06-16 23:41:00'),
(91, 4600, '2014-06-16 23:41:00', '2014-06-16 23:41:00'),
(92, 4650, '2015-06-16 23:41:00', '2015-06-16 23:41:00'),
(93, 4700, '2016-06-16 23:41:00', '2016-06-16 23:41:00'),
(94, 4750, '2017-06-16 23:41:00', '2017-06-16 23:41:00'),
(95, 4800, '2018-06-16 23:41:00', '2018-06-16 23:41:00'),
(96, 4850, '2019-06-16 23:41:00', '2019-06-16 23:41:00'),
(97, 4900, '2020-06-16 23:41:00', '2020-06-16 23:41:00'),
(98, 4950, '2021-06-16 23:41:00', '2021-06-16 23:41:00'),
(99, 5000, '2022-06-16 23:41:00', '2022-06-16 23:41:00'),
(100, 5050, '2023-06-16 23:41:00', '2023-06-16 23:41:00');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `chat_user_id` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `seen` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `chat_user_id`, `agent_id`, `message`, `seen`, `created_at`, `updated_at`) VALUES
(1, 15, 1, 'Say Hi Golf_2', 1, '2017-07-18 15:25:37', '2017-07-18 15:25:37'),
(2, 15, 1, 'i love you', 1, '2017-07-18 15:26:52', '2017-07-18 15:26:52'),
(3, 15, 1, 'i love you', 1, '2017-07-18 15:29:57', '2017-07-18 15:29:57'),
(4, 15, 1, 'ทดลองดูเฉยๆจ้า', 1, '2017-07-18 15:30:15', '2017-07-18 15:30:15'),
(5, 15, 1, 'ทดลองดูเฉยๆจ้า', 1, '2017-07-18 15:32:30', '2017-07-18 15:32:30'),
(6, 15, 1, 'ทดลองดูเฉยๆจ้า', 1, '2017-07-18 15:33:01', '2017-07-18 15:33:01'),
(7, 15, 1, 'ทดลองดูเฉยๆจ้า', 1, '2017-07-18 15:33:30', '2017-07-18 15:33:30');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2017_01_06_063612_create_social_accounts_table', 1),
('2017_01_11_164155_create_courses_table', 1),
('2017_01_11_174927_create_typecourses_table', 1),
('2017_03_16_035836_create_user_confirms_table', 2),
('2017_03_25_104805_create_submitcourses_table', 3),
('2017_03_25_135037_create_banks_table', 4),
('2017_04_11_140828_add_hrcourse_to_submitcourses', 5),
('2017_04_13_181652_add_discount_to_submitcourses', 6),
('2017_04_13_181857_add_discount_to_courses', 7),
('2017_04_14_193331_create_comments_table', 8),
('2017_04_16_134106_create_contacts_table', 9),
('2017_04_18_124328_create_blogs_table', 10),
('2017_04_19_081642_add_view_to_blogs', 11),
('2017_04_23_134933_create_questions_table', 12),
('2017_04_23_134949_create_options_table', 12),
('2017_04_23_182844_add_point_to_questions', 13),
('2017_04_23_193623_add_status_to_questions', 14),
('2017_04_29_103205_create_categories_table', 15),
('2017_04_29_110903_create_examples_table', 16),
('2017_04_29_195706_add_code_couese_to_courses', 17),
('2017_05_17_093148_create_answers_table', 18),
('2017_05_23_023516_create_messages_table', 19),
('2017_05_28_142407_create_departments_table', 20),
('2017_05_28_142638_add_department_id_to_courses', 20),
('2017_06_22_222452_create_setpoints_table', 21),
('2017_06_30_130442_create_levels_table', 22),
('2017_08_07_063511_create_video_lists_table', 23);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id_option` int(10) UNSIGNED NOT NULL,
  `question_id` int(11) NOT NULL,
  `name_option` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_option` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id_option`, `question_id`, `name_option`, `type_option`, `created_at`, `updated_at`) VALUES
(1, 1, 'เสือ', '2', '2017-06-21 04:49:08', '2017-06-21 04:49:08'),
(2, 1, 'ปลา', '2', '2017-06-21 04:49:08', '2017-06-21 04:49:08'),
(3, 1, 'น้ำเต้า', '2', '2017-06-21 04:49:08', '2017-06-21 04:49:08'),
(4, 1, 'แมว', '2', '2017-06-21 04:49:08', '2017-06-21 04:49:08'),
(5, 2, 'กุ้ง', '2', '2017-06-21 04:58:52', '2017-06-21 04:58:52'),
(6, 2, 'หอย', '2', '2017-06-21 04:58:52', '2017-06-21 04:58:52'),
(7, 2, 'ปู', '2', '2017-06-21 04:58:52', '2017-06-21 04:58:52'),
(8, 2, 'ปลา', '2', '2017-06-21 04:58:52', '2017-06-21 04:58:52'),
(9, 3, '', '1', '2017-06-26 20:04:36', '2017-06-26 20:04:36'),
(10, 4, '', '1', '2017-06-26 20:04:47', '2017-06-26 20:04:47'),
(11, 5, '', '1', '2017-06-26 20:04:58', '2017-06-26 20:04:58'),
(12, 6, '', '1', '2017-06-26 20:05:10', '2017-06-26 20:05:10'),
(13, 7, '', '1', '2017-06-26 20:05:28', '2017-06-26 20:05:28');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('kim.kundad@gmail.com', 'c7c40475f90b9c5c1a8b3d666bde317dfd579091385b28ce26012c7c777cc13a', '2017-03-25 14:23:36');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id_questions` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `name_questions` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_sort` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `point` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id_questions`, `category_id`, `name_questions`, `order_sort`, `created_at`, `updated_at`, `point`, `status`) VALUES
(1, 8, 'สัตว์ชนิดไหนเป็นสัตว์น้ำ', 2, '2017-06-21 04:49:08', '2017-06-21 04:49:08', 1, 2),
(2, 8, 'น้องแมวชอบกินไร', 1, '2017-06-21 04:58:51', '2017-06-21 04:58:51', 1, 8),
(3, 9, '1+2=?', 1, '2017-06-26 20:04:36', '2017-06-26 20:04:36', 1, 0),
(4, 9, '3+5=?', 2, '2017-06-26 20:04:47', '2017-06-26 20:04:47', 1, 0),
(5, 9, '8+6=?', 3, '2017-06-26 20:04:58', '2017-06-26 20:04:58', 1, 0),
(6, 9, '9-9=?', 4, '2017-06-26 20:05:10', '2017-06-26 20:05:10', 1, 0),
(7, 9, '1x8=?', 5, '2017-06-26 20:05:28', '2017-06-26 20:05:28', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `setpoints`
--

CREATE TABLE `setpoints` (
  `id` int(10) UNSIGNED NOT NULL,
  `examples_id_p` int(11) NOT NULL,
  `id_option_p` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status_p` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `setpoints`
--

INSERT INTO `setpoints` (`id`, `examples_id_p`, `id_option_p`, `user_id`, `status_p`, `created_at`, `updated_at`) VALUES
(1, 9, 13, 16, 0, '2017-06-26 20:06:44', '2017-06-26 20:06:44'),
(2, 9, 14, 16, 0, '2017-06-26 20:08:56', '2017-06-26 20:08:56'),
(3, 9, 15, 16, 1, '2017-06-27 01:52:30', '2017-06-29 04:26:15');

-- --------------------------------------------------------

--
-- Table structure for table `social_accounts`
--

CREATE TABLE `social_accounts` (
  `user_id` int(11) NOT NULL,
  `provider_user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `social_accounts`
--

INSERT INTO `social_accounts` (`user_id`, `provider_user_id`, `provider`, `created_at`, `updated_at`) VALUES
(9, '1168382209906342', 'facebook', '2017-04-02 23:32:17', '2017-04-02 23:32:17'),
(10, '1494929977192662', 'facebook', '2017-04-14 13:40:48', '2017-04-14 13:40:48');

-- --------------------------------------------------------

--
-- Table structure for table `submitcourses`
--

CREATE TABLE `submitcourses` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `end_time` int(11) DEFAULT NULL,
  `end_day` date DEFAULT NULL,
  `status` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `money_tran` int(11) NOT NULL,
  `date_tran` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time_tran` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `hrcourse` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `bill_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `submitcourses`
--

INSERT INTO `submitcourses` (`id`, `user_id`, `course_id`, `end_time`, `end_day`, `status`, `bank_id`, `money_tran`, `date_tran`, `time_tran`, `created_at`, `updated_at`, `hrcourse`, `discount`, `bill_image`) VALUES
(2, 15, 4, 0, '2017-05-10', 2, 2, 1300, '2017-05-10', '10.30', '2017-05-07 15:13:13', '2017-05-08 05:33:21', 3600, 0, NULL),
(3, 15, 5, 0, '0000-00-00', 2, 2, 1200, '12', '12', '2017-05-20 03:00:05', '2017-05-20 03:00:14', 3600, 0, NULL),
(4, 16, 4, 0, '0000-00-00', 2, 2, 1500, '12', '12', '2017-06-26 01:20:13', '2017-06-26 01:20:23', 3600, 0, NULL),
(6, 16, 5, 0, '0000-00-00', 1, 2, 1300, '12', '12', '2017-07-12 08:42:48', '2017-07-12 08:43:13', 0, 0, '1499874192.jpg'),
(7, 19, 5, 0, '0000-00-00', 1, 3, 1500, '2018-05-29', '10.25', '2018-05-08 02:09:19', '2018-05-08 02:12:08', 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `typecourses`
--

CREATE TABLE `typecourses` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `typecourses`
--

INSERT INTO `typecourses` (`id`, `type_name`, `created_at`, `updated_at`) VALUES
(1, 'คอร์สสอนสด', '2017-03-16 15:54:02', '2017-04-15 02:26:41'),
(2, 'คอร์สเรียนออนไลน์', '2017-03-17 00:57:10', '2017-04-15 02:26:56');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'blank_avatar_240x240.gif',
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'email',
  `bio` text COLLATE utf8_unicode_ci NOT NULL,
  `hbd` date NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `line_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_admin` tinyint(1) NOT NULL,
  `point_level` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `position`, `avatar`, `provider`, `bio`, `hbd`, `phone`, `address`, `line_id`, `is_admin`, `point_level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '$2y$10$vr6046CoL0HyVI0427Nr..RzUSoEhj9mpT5TR9SBgUBy8Gi5wDRcm', 'admin', '', '', '', '0000-00-00', '', '', '', 1, 0, 'hYiIwsJ6bElYNwpiqBDU8LjeA9MgYCNu0bMEvxDWHsiHd5IIkYtf1iZVdEjB', '2017-03-11 01:14:39', '2017-08-24 05:02:41'),
(9, 'Shuvit Funsok', 'ighostzaa@gmail.com', '', 'student', 'graph.facebook.com/1168382209906342/picture?width=300&height=300', 'facebook', '', '1901-01-02', '0811007753', '5551221', 'kimkundad', 0, 0, 'PMDmLNWG8hAJexSex5V6h564pWqLORsx79NjYGDRSIwzKmPrkopS1blnJQ5t', '2017-04-02 23:32:17', '2018-05-09 05:35:43'),
(10, 'Worphon Tr Noppakun', 'noppakun_w@hotmail.com', '', 'student', 'graph.facebook.com/1494929977192662/picture?width=300&height=300', 'facebook', '', '0000-00-00', '', '', '', 0, 0, NULL, '2017-04-14 13:40:48', '2017-04-14 13:40:48'),
(15, 'kimkundad naja', 'info@gmail.com', '$2y$10$AjEFg0V5KOEwiEe1Z1O1ruTEeEwPLdNr1EB08QKftF7cPgKt6eotG', '', '1495714035.png', 'email', '12', '1987-07-16', '0811007753', '12', 'kimkundad', 0, 50, 'CjNZdlg9gnTaEx0GF7pq34tTnJcaWEvxfimckSsbLgCYygAdliys4RlXm29X', '2017-05-07 15:02:31', '2017-06-26 01:19:17'),
(16, 'kim', 'kim@gmail.com', '$2y$10$UxDZSvPqdbSaUI9CbMedqua1isukPtYLqwBXfR.zkOo155JUQdPDW', 'student', '1498825471.png', 'email', '555', '1901-01-09', '0811007753', '5555', 'kim', 0, 452, 'HnWqOMi8NCefqDXnpxJE3vqFzMBtahLd6fFzkUqcuWPtbLlQRcvuoC4sBqJA', '2017-05-25 02:15:25', '2017-06-30 06:13:51'),
(17, 'E-commerce', 'admin7@gmail.com', '$2y$10$QXqsXQua3EmGU8/Aij4bQeOserYXENlkVoWoiOc5sRTyIC2YtT6Ny', 'student', 'blank_avatar_240x240.gif', 'email', '', '0000-00-00', '', '', '', 0, 0, 'CUyq7E2VcB7VdqVq7rrxZYa6RIjEp1A6OW5LsttyKHq2alD3aRiJgxVSOPCY', '2017-08-24 05:03:52', '2017-08-24 05:08:22'),
(18, 'slide1', 'info@scb.com', '$2y$10$UfVUPjGtwyBqrmsnphrUqO92stT1MaD6MY7Z/Equ15pgoE8iU/XmS', 'student', 'blank_avatar_240x240.gif', 'email', '', '0000-00-00', '', '', '', 0, 0, 'mAji7WfaS8W8Yh2nmiaJDQMcxouJ4EjxfBjDbaudqNUHIQXjwlunk3eo9zx3', '2017-08-26 01:40:12', '2017-08-26 01:40:12'),
(19, 'kim', 'kim.kundad@gmail.com', '$2y$10$ew1mI/TUwOl9azJyAW3zZeorkMuG7jWts5vGdj6vcvfYTA8v2/TaO', 'student', 'blank_avatar_240x240.gif', 'email', '', '0000-00-00', '0811007753', 'ปทุมวัน/ Pathum Wan in กรุงเทพมหานคร/ Bangkok, 10110', '', 0, 0, 'cdtYKuNCxkEfji9tABhfgP7WZAn2dhLLI8tXcD40X6CgPjcULk56ekcGtme9', '2018-05-07 11:45:23', '2018-05-08 14:42:19');

-- --------------------------------------------------------

--
-- Table structure for table `user_confirms`
--

CREATE TABLE `user_confirms` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_card` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_confirms`
--

INSERT INTO `user_confirms` (`id`, `user_id`, `image`, `id_card`, `status`, `created_at`, `updated_at`) VALUES
(2, 6, '1489646569.jpg', '1101401049377', 1, '2017-03-15 23:41:45', '2017-03-15 23:42:57'),
(3, 5, '1489896585.png', '1101401049378', 0, '2017-03-18 21:09:45', '2017-03-18 21:09:45'),
(4, 9, '1492980246.jpg', '1101401049378', 0, '2017-04-23 13:44:07', '2017-04-23 13:44:07');

-- --------------------------------------------------------

--
-- Table structure for table `video_lists`
--

CREATE TABLE `video_lists` (
  `id` int(10) UNSIGNED NOT NULL,
  `course_id` int(11) NOT NULL,
  `course_video_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `course_video` text COLLATE utf8_unicode_ci NOT NULL,
  `order_sort` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `examples`
--
ALTER TABLE `examples`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id_option`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id_questions`);

--
-- Indexes for table `setpoints`
--
ALTER TABLE `setpoints`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `submitcourses`
--
ALTER TABLE `submitcourses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `typecourses`
--
ALTER TABLE `typecourses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_confirms`
--
ALTER TABLE `user_confirms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video_lists`
--
ALTER TABLE `video_lists`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `examples`
--
ALTER TABLE `examples`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `levels`
--
ALTER TABLE `levels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id_option` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id_questions` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `setpoints`
--
ALTER TABLE `setpoints`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `submitcourses`
--
ALTER TABLE `submitcourses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `typecourses`
--
ALTER TABLE `typecourses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `user_confirms`
--
ALTER TABLE `user_confirms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `video_lists`
--
ALTER TABLE `video_lists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
